# docker-sh
Linux script to select a running container you wish to quickly start a shell inside. Convenient for terminal users to quickly spin up terminals for any running docker containers.

## Installation
```sh
cp docker.sh /usr/share/
chmod +x /usr/share/docker.sh
```